'use strict';

const Get = require('../generic/get');

module.exports = (parent, args) => Get('posts', args.id);
