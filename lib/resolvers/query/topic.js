'use strict';

const Get = require('../generic/get');

module.exports = (parent, args) => Get('topics', args.id);
