# Contributing to Forum

At this moment Forum is a toy project intended to experiment with ideas
for an internet forum based on the past's future.

## The objective of Forum

The main objective is to create a Forum that we would be proud to use in
a 90s interpretation of the internet of tomorrow. Any contribution that
would push in that direction, or improve the code or processes around it
is welcome! :)

Some ideas so far include:

* Ephemeral posts
* Physical tokens for login
* Magic glyphs generated from username
* Posts can be categorized by forum or tag, or be completely unlisted
* Flagging with option to publish/subscribe to flags
* Signatures

## How to contribute

Above All: Be nice, always.

* Ensure the style checkers show no warnings or errors
* Don't break the CI
* Make the PRs according to [Git Flow][gitflow]: (features go to
  develop, hotfixes go to master)

[gitflow]: https://github.com/nvie/gitflow
