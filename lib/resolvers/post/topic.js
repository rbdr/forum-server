'use strict';

const Get = require('../generic/get');

module.exports = (parent) => Get('topics', parent.topic_id);
