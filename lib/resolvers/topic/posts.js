'use strict';

const GetAllCompound = require('../generic/get_all_compound');

module.exports = (parent) => GetAllCompound('posts', 'topic_id-created_at', parent.id, { order: 'asc' });
