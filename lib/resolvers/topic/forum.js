'use strict';

const Get = require('../generic/get');

module.exports = (parent) => Get('forums', parent.forum_id);
