'use strict';

const { GraphQLTimestamp } = require('graphql-scalars');
const Forums = require('./query/forums');
const Forum = require('./query/forum');
const Post = require('./query/post');
const Tag = require('./query/tag');
const Topic = require('./query/topic');
const ForumTopics = require('./forum/topics');
const TagTopics = require('./tag/topics');
const TopicForum = require('./topic/forum');
const TopicTags = require('./topic/tags');
const TopicPosts = require('./topic/posts');
const PostAuthor = require('./post/author');
const PostTopic = require('./post/topic');

module.exports = {
  Timestamp: GraphQLTimestamp,
  Query: {
    post: Post,
    forum: Forum,
    forums: Forums,
    tag: Tag,
    topic: Topic
  },
  Forum: {
    topics: ForumTopics
  },
  Tag: {
    topics: TagTopics
  },
  Topic: {
    forum: TopicForum,
    tags: TopicTags,
    posts: TopicPosts
  },
  Post: {
    author: PostAuthor,
    topic: PostTopic
  }
};
