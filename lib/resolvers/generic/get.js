'use strict';

const RethinkDB = require('rethinkdb');
const { getConnectionWithDatabase } = require('../../../db/helpers');

module.exports = async function forums(table, key) {

  if (!key) {
    return null;
  }

  const connection = await getConnectionWithDatabase();
  const cursor = await RethinkDB
    .table(table)
    .get(key)
    .run(connection);

  return await cursor;
};

