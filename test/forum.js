'use strict';

const { expect } = require('@hapi/code');
const Lab = require('@hapi/lab');
const Forum = require('../lib/forum');

const { it, describe } = exports.lab = Lab.script();

describe('Forum Backend Library', () => {

  it('Should initialize the server', () => {

    expect(Forum).not.to.equal(undefined);
  });
});
