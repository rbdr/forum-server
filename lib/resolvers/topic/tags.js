'use strict';

module.exports = function forums(parent, args, context, info) {

  if (!parent.tags) {
    return [];
  }

  return Object.entries(parent.tags).map((entry) => ({ id: entry[0], weight: entry[1] }));
};

