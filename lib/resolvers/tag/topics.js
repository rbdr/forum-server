'use strict';

const GetAllCompound = require('../generic/get_all_compound');

module.exports = (parent) => GetAllCompound('topics', 'tags', parent.id);
