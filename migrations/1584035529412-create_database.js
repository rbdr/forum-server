'use strict';

const RethinkDB = require('rethinkdb');
const { rethinkDB } = require('../config/config');
const { getConnection } = require('../db/helpers');

module.exports.up = async function () {

  const connection = await getConnection();
  await RethinkDB.dbCreate(rethinkDB.database).run(connection);
};

module.exports.down = async function () {

  const connection = await getConnection();
  await RethinkDB.dbDrop(rethinkDB.database).run(connection);
};
