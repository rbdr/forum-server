# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]
### Added
- Svelte based frontend
- Koa based backend
- RethinkDB for backend
- This CHANGELOG
- A README
- A CONTRIBUTING guide
- Linter configuration for JS and HTML
- Git hook to lint code before commit
- NPM tasks to lint and install git hooks
- A gitignore file

[Unreleased]: https://gitlab.com/rbdr/forum/compare/master...develop
