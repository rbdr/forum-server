'use strict';

const RethinkDB = require('rethinkdb');
const { getConnectionWithDatabase } = require('../db/helpers');

const internals = {
  kTableName: 'topics',
  kIndexName: 'tags',
  kDateName: 'updated_at'
};

module.exports.up = async function () {

  const connection = await getConnectionWithDatabase();
  await RethinkDB.table(internals.kTableName).indexCreate(internals.kIndexName, (row) => {

    return row(internals.kIndexName).keys().map((key) => [key, row(internals.kDateName)]);
  }, { multi: true }).run(connection);
};

module.exports.down = async function () {

  const connection = await getConnectionWithDatabase();
  await RethinkDB.table(internals.kTableName).indexDrop(internals.kIndexName).run(connection);
};
