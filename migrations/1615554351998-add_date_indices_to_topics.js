'use strict';

const RethinkDB = require('rethinkdb');
const { getConnectionWithDatabase } = require('../db/helpers');

const internals = {
  kTableName: 'topics',
  kPrimaryName: 'forum_id',
  kIndexNames: ['created_at', 'updated_at', 'ttl']
};

module.exports.up = async function () {

  const connection = await getConnectionWithDatabase();
  for (const indexName of internals.kIndexNames) {
    await RethinkDB.table(internals.kTableName).indexCreate(internals.kPrimaryName + '-' + indexName, [
      RethinkDB.row(internals.kPrimaryName),
      RethinkDB.row(indexName)
    ]).run(connection);
  }
};

module.exports.down = async function () {

  const connection = await getConnectionWithDatabase();
  for (const indexName of internals.kIndexNames) {
    await RethinkDB.table(internals.kTableName).indexDrop(internals.kPrimaryName + '-' + indexName).run(connection);
  }
};
