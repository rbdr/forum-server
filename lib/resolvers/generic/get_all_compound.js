'use strict';

const internals = {
  kTableName: 'topics',
  kIndexName: 'tags',
  kLimitAmount: 50
};

const RethinkDB = require('rethinkdb');
const { getConnectionWithDatabase } = require('../../../db/helpers');

/*
 * Order should be eitther desc or asc
 */
module.exports = async function topics(table, index, key, { limit = 50, order = 'desc' } = {}) {

  const connection = await getConnectionWithDatabase();
  const cursor = await RethinkDB
    .table(table)
    .orderBy({ index: RethinkDB[order](index) })
    .between([key, RethinkDB.minval],
      [key, RethinkDB.maxval],
      { index })
    .slice(0, limit)
    .run(connection);

  return await cursor.toArray();
};
