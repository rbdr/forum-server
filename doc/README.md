## Modules

<dl>
<dt><a href="#module_Forum">Forum</a></dt>
<dd><p>The Forum class is the main entry point for the backend application.</p>
</dd>
</dl>

## Typedefs

<dl>
<dt><a href="#tForumBackendConfiguration">tForumBackendConfiguration</a> : <code>object</code></dt>
<dd><p>The main configuration object for the Forum backend. It will be used to
initialize all of the sub-components. It can extend any property of
the forum object.</p>
</dd>
<dt><a href="#tJWTConfiguration">tJWTConfiguration</a> : <code>object</code></dt>
<dd><p>Configures the behavior of the JWT token.</p>
</dd>
<dt><a href="#tRethinkDBConfiguration">tRethinkDBConfiguration</a> : <code>object</code></dt>
<dd><p>Information required to connect to the rethinkDB server</p>
</dd>
</dl>

<a name="module_Forum"></a>

## Forum
The Forum class is the main entry point for the backend application.


| Param | Type | Description |
| --- | --- | --- |
| config | [<code>tForumBackendConfiguration</code>](#tForumBackendConfiguration) | the initialization options to extend the instance |

<a name="module_Forum+run"></a>

### forum.run()
Initializes the application and starts listening. Also prints a
nice robotic banner with information.

**Kind**: instance method of [<code>Forum</code>](#module_Forum)  
<a name="tForumBackendConfiguration"></a>

## tForumBackendConfiguration : <code>object</code>
The main configuration object for the Forum backend. It will be used to
initialize all of the sub-components. It can extend any property of
the forum object.

**Kind**: global typedef  
**Properties**

| Name | Type | Default | Description |
| --- | --- | --- | --- |
| [port] | <code>number</code> | <code>1978</code> | the port where the app will listen on |
| [staticDirectory] | <code>string</code> | <code>&quot;static&quot;</code> | the path, relative to the project root, where static assets live |
| [ttl] | <code>number</code> | <code>180</code> | the time in seconds that posts remain alive |
| rethinkDB | [<code>tRethinkDBConfiguration</code>](#tRethinkDBConfiguration) |  | the configuration to connect to the rethinkDB server |
| jwt | [<code>tJWTConfiguration</code>](#tJWTConfiguration) |  | the configuration for the JWT authentication |

<a name="tJWTConfiguration"></a>

## tJWTConfiguration : <code>object</code>
Configures the behavior of the JWT token.

**Kind**: global typedef  
**Properties**

| Name | Type | Default | Description |
| --- | --- | --- | --- |
| [duration] | <code>number</code> | <code>86400</code> | the duration of the JWT in seconds |
| secret | <code>string</code> |  | the secret used to sign the JWT |

<a name="tRethinkDBConfiguration"></a>

## tRethinkDBConfiguration : <code>object</code>
Information required to connect to the rethinkDB server

**Kind**: global typedef  
**Properties**

| Name | Type | Default | Description |
| --- | --- | --- | --- |
| host | <code>string</code> |  | the location of the rethinkDB host |
| [post] | <code>string</code> | <code>6379</code> | port where rethinkDB server is listening |

