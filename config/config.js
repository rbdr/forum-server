'use strict';

const Getenv = require('getenv2');
const Joi = require('joi');

/**
 * The main configuration object for the Forum backend. It will be used to
 * initialize all of the sub-components. It can extend any property of
 * the forum object.
 *
 * @typedef {object} tForumBackendConfiguration
 * @property {number} [port=1978] the port where the app will listen on
 * @property {number} [ttl=180] the time in seconds that posts
 * remain alive
 * @property {tRethinkDBConfiguration} rethinkDB the configuration to
 * connect to the rethinkDB server
 * @property {tJWTConfiguration} jwt the configuration for the
 * JWT authentication
 */
module.exports = {
  port: Getenv('FORUM_PORT', Joi.number().integer(), 1978),
  ttl: Getenv('FORUM_TTL', Joi.number().integer(), 180),

  /**
   * Configures the behavior of the JWT token.
   *
   * @typedef {object} tJWTConfiguration
   * @property {number} [duration=86400] the duration of the JWT in
   * seconds
   * @property {string} secret the secret used to sign the JWT
   */
  jwt: {
    duration: Getenv('FORUM_JWT_DURATION', Joi.number().integer(), 86400),
    secret: Getenv('FORUM_JWT_SECRET', Joi.string())
  },

  /**
   * Information required to connect to the rethinkDB server
   *
   * @typedef {object} tRethinkDBConfiguration
   * @property {string} host the location of the rethinkDB host
   * @property {string} [post=6379] port where rethinkDB server is listening
   */
  rethinkDB: {
    host: Getenv('FORUM_RETHINK_DB_HOST', Joi.string()),
    port: Getenv('FORUM_RETHINK_DB_PORT', Joi.number().integer(), 28015),
    database: Getenv('FORUM_RETHINK_DB_DATABASE', Joi.string(), 'forum')
  }
};
