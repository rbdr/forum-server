'use strict';

const Fastify = require('fastify');
const { ApolloServer } = require('apollo-server-fastify');
const Schema = require('../db/schema.graphql');
const Resolvers = require('./resolvers');

/**
 * The Forum class is the main entry point for the backend application.
 *
 * @module Forum
 * @param {tForumBackendConfiguration} config the initialization options to
 * extend the instance
 */
module.exports = class Forum {

  constructor(config) {

    Object.assign(this, config);
  }

  /**
   * Initializes the application and starts listening. Also prints a
   * nice robotic banner with information.
   */
  async run() {

    const server = this._initializeServer();
    this._initializeApollo(server);

    server.get('/', async (request, reply) => {

      return await { status: 'ok' };
    });

    await this._startServer(server);
    this._printBanner();
  }

  // Initializes the http server

  _initializeServer(app) {

    return Fastify({
      logger: true
    });
  }

  // Initializes the WebSocket server

  _initializeApollo(server) {

    const apollo = new ApolloServer({
      typeDefs: Schema,
      resolvers: Resolvers
    });
    server.register(apollo.createHandler());
  }

  // Starts listening

  async _startServer(server) {

    await server.listen(this.port, '0.0.0.0');
  }

  // Prints the banner.

  _printBanner() {

    console.log('        .');
    console.log('       /');
    console.log('    +-----+');
    console.log(`    | o o |  - Listening Gladly, Try me on port: ${this.port}`);
    console.log('    +-----+');
    console.log('  +---------+');
    console.log(' /|    [][] |\\');
    console.log(' ||         | |');
    console.log(' ||         |  \\c');
    console.log(' ^+---------+');
    console.log('      (.) ');
  }
};
