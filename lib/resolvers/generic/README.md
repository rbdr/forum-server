# Generic Resolvers

These are re-usable resolvers for the most common cases:

* get: Fetch one by index.
* get_all_compound: Get all using a compound index
