'use strict';

const Get = require('../generic/get');

module.exports = (parent) => Get('users', parent.author_id);
