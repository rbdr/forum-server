FROM node:15-alpine

WORKDIR /app

COPY package-lock.json /app/
COPY package.json /app/
RUN npm install
COPY . /app

ENTRYPOINT [ "node", "bin/forum" ])
