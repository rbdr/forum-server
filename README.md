# Forum Server

A forum server for the year 3000.

## About Forum Server

This project is the service that provides the data for [forum][forum].
It exposes a GraphQL service under the `/graphql` endpoint.

## Usage Guide for Frontend Applications

### Connecting

Configure your client to use the `/graphql` endpoint.

```javascript
const apollo = new ApolloClient({
  uri: 'http://localhost:1978/graphql'
  // The rest of the config
});
```

## Setup Guide for Developers

### What you will need to get started

This project uses [node][node] for most of its development processes.
Once you have node installed according to the instructions there, run
`npm install` from the root of the project. This will install the required
project dependencies.

To run the server, this project uses [docker][docker]. Once you have
docker installed according to the instructions there, check out the
instructions under "Running the server"

### Running the server

This project provides a [docker compose][docker-compose] configuration
to easily run the service. In order to run the server, run
`docker-compose up`. This will run two services: one containing the
forum server itself, and another one using the database.

### Code Style

This project follows the [Hapi Styleguide][hapi-styleguide] and uses
[eslint][eslint] to enforce it. If you install the included git hooks
it will check eslint before every commit. And on every push, it will
run a build to check whether the style guide is followed. Otherwise
merges will not be accepted.

### Installing git hooks

This repo contains a pre-commit git hook so eslint will run before every
commit. Run `npm run setup-hooks` to install it.

### Running migrations on the docker environment

In order to run migrations on the docker environment, you should run
`npm run docker:migrate`.

If you only want to seed the database, you should run `npm run docker:seed`.

To roll back migrations you can run `docker:migrate:down` followed by the
migration you want to roll back to (eg. `docker:migrate:down 000000_add_table`)

### Accessing the database control panel

This project uses rethinkdb as a database, which exposes an
admin panel on port 8080. You can open it in a browser by
running `npm run db:admin` which will open `http://localhost:8080`

### Accessing the graphql control panel

You can run queries on the server using the provided graphical
interface by accessing `http://localhost:1978/graphql` in your
browser. You can also do this by running `npm run graphql:admin`.

## Development Guide

### Creating migrations for the database

This project uses `migrate` to create and run migrations in the
database. If you want to create, modify or remove a table you
should first create a migration using `npx migrate create <migration_name>`
where the `<migration_name>` describes the operation you want to do using
the imperative mood. Here's some examples:
  * `npx migrate create add_posts_table`: Add a table called posts
  * `npx migrate create add_forum_id_to_topics`: Add a `forum_id` column to
    the `topics` table
  * `npx migrate create add_position_index_to_forums`: Adds an index called
    position to the forums table
  * `npx migrate create remove_posts_table`: Removes a table called posts

Migrations will be created in the `migrations` directory with a timestamp.
They will run from oldest to newest when running `npm run db:migrate`.

Inside these files you will find an `up` and `down` function. `up` should
perform the main action of the migration (eg. for `add_posts_table` it should
have the code to create that table), and the `down` function should have
the opposite action (eg. in the previous example, it would have code to
remove that table)

The file `db/helpers.js` has a set of helper functions for some common
operations used when dealing with the database: gettting a connection
to the database, and getting a connection with a database selected.

[docker]: https://www.docker.com/
[docker-compose]: https://docs.docker.com/compose/
[eslint]: https://eslint.org/
[forum]: https://gitlab.com/rbdr/forum
[hapi-styleguide]: https://hapi.dev/policies/styleguide/
[node]: https://nodejs.org/en/
[rethinkdb-change]: https://rethinkdb.com/api/javascript/changes
