'use strict';

const RethinkDB = require('rethinkdb');
const { getConnectionWithDatabase } = require('../db/helpers');

const internals = {
  kTableName: 'forums'
};

module.exports.up = async function () {

  const connection = await getConnectionWithDatabase();
  await RethinkDB.tableCreate(internals.kTableName).run(connection);
};

module.exports.down = async function () {

  const connection = await getConnectionWithDatabase();
  await RethinkDB.tableDrop(internals.kTableName).run(connection);
};
