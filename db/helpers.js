'use strict';

const RethinkDB = require('rethinkdb');
const { rethinkDB } = require('../config/config');

const internals = {

  connection: null,

  async establishConnection() {

    internals.connection = await RethinkDB.connect(rethinkDB);

    internals.connection.on('close', () => {

      internals.connection = null;
    });

    return internals.connection;
  },

  // Get database connection

  async getConnection() {

    if (internals.connection) {
      return internals.connection;
    }

    return await internals.establishConnection();
  },

  // Get database connection, using the DB already

  async getConnectionWithDatabase() {

    const connection = await internals.getConnection();
    connection.use(rethinkDB.database);

    return connection;
  }
};

/*
 * Helpers for database migrations and seeding. It contains
 * helper methods to fetch connections and do repetitive code.
 */
module.exports = {
  getConnection: internals.getConnection,
  getConnectionWithDatabase: internals.getConnectionWithDatabase
};
