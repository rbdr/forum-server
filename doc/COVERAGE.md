# Code Quality Report  
Thu Nov 15 2018 01:45:42 GMT+0100 (Central European Standard Time)  
  
## Tests
    
**Application Loader**  
✔ 1) Should instantiate and start Forum on window load (18 ms)  
  
  
1 test  
0 tests failed  
0 tests skipped  
  
Test duration: 27 ms  
  
  
## Leaks  
The following global variable leaks were detected:_registeredHandlers, _eventHandlers, DOMException, NamedNodeMap, Attr, Node, Element, DocumentFragment, HTMLDocument, Document, XMLDocument, CharacterData, Text, CDATASection, ProcessingInstruction, Comment, DocumentType, DOMImplementation, NodeList, HTMLCollection, HTMLOptionsCollection, DOMStringMap, DOMTokenList, SVGAnimatedString, SVGNumber, SVGStringList, Event, CloseEvent, CustomEvent, MessageEvent, ErrorEvent, HashChangeEvent, FocusEvent, PopStateEvent, UIEvent, MouseEvent, KeyboardEvent, TouchEvent, ProgressEvent, CompositionEvent, WheelEvent, EventTarget, BarProp, Location, History, Screen, Performance, Blob, File, FileList, DOMParser, FormData, XMLHttpRequestEventTarget, XMLHttpRequestUpload, NodeIterator, TreeWalker, HTMLElement, HTMLAnchorElement, HTMLAreaElement, HTMLAudioElement, HTMLBaseElement, HTMLBodyElement, HTMLBRElement, HTMLButtonElement, HTMLCanvasElement, HTMLDataElement, HTMLDataListElement, HTMLDetailsElement, HTMLDialogElement, HTMLDirectoryElement, HTMLDivElement, HTMLDListElement, HTMLEmbedElement, HTMLFieldSetElement, HTMLFontElement, HTMLFormElement, HTMLFrameElement, HTMLFrameSetElement, HTMLHeadingElement, HTMLHeadElement, HTMLHRElement, HTMLHtmlElement, HTMLIFrameElement, HTMLImageElement, HTMLInputElement, HTMLLabelElement, HTMLLegendElement, HTMLLIElement, HTMLLinkElement, HTMLMapElement, HTMLMarqueeElement, HTMLMediaElement, HTMLMenuElement, HTMLMetaElement, HTMLMeterElement, HTMLModElement, HTMLObjectElement, HTMLOListElement, HTMLOptGroupElement, HTMLOptionElement, HTMLOutputElement, HTMLParagraphElement, HTMLParamElement, HTMLPictureElement, HTMLPreElement, HTMLProgressElement, HTMLQuoteElement, HTMLScriptElement, HTMLSelectElement, HTMLSourceElement, HTMLSpanElement, HTMLStyleElement, HTMLTableCaptionElement, HTMLTableCellElement, HTMLTableColElement, HTMLTableElement, HTMLTimeElement, HTMLTitleElement, HTMLTableRowElement, HTMLTableSectionElement, HTMLTemplateElement, HTMLTextAreaElement, HTMLTrackElement, HTMLUListElement, HTMLUnknownElement, HTMLVideoElement, SVGElement, SVGGraphicsElement, SVGSVGElement, StyleSheet, MediaList, CSSStyleSheet, CSSRule, CSSStyleRule, CSSMediaRule, CSSImportRule, CSSStyleDeclaration, StyleSheetList, XPathException, XPathExpression, XPathResult, XPathEvaluator, NodeFilter, Window, _globalProxy, _document, _sessionHistory, _virtualConsole, _runScripts, _top, _parent, _frameElement, _length, _pretendToBeVisual, length, window, frameElement, frames, self, parent, top, document, external, location, history, navigator, locationbar, menubar, personalbar, scrollbars, statusbar, toolbar, performance, screen, addEventListener, removeEventListener, dispatchEvent, __stopAllTimers, Option, Image, Audio, postMessage, atob, btoa, FileReader, WebSocket, AbortSignal, AbortController, XMLHttpRequest, stop, close, getComputedStyle, captureEvents, releaseEvents, name, devicePixelRatio, innerWidth, innerHeight, outerWidth, outerHeight, pageXOffset, pageYOffset, screenX, screenY, scrollX, scrollY, screenLeft, screenTop, alert, blur, confirm, focus, moveBy, moveTo, open, print, prompt, resizeBy, resizeTo, scroll, scrollBy, scrollTo  
  
  
## Coverage  
Threshold: 100%  
Coverage: 0.00% (0/0)  
  
  
  
## Linting  
Warnings threshold: 0  
Errors threshold: 0  
No issues  
  
