'use strict';

const RethinkDB = require('rethinkdb');
const { getConnectionWithDatabase } = require('../db/helpers');

const internals = {
  kTableName: 'posts',
  kPrimaryName: 'topic_id',
  kDateName: 'created_at'
};

module.exports.up = async function () {

  const connection = await getConnectionWithDatabase();
  await RethinkDB.table(internals.kTableName).indexCreate(internals.kPrimaryName + '-' + internals.kDateName, [
    RethinkDB.row(internals.kPrimaryName),
    RethinkDB.row(internals.kDateName)
  ]).run(connection);
};

module.exports.down = async function () {

  const connection = await getConnectionWithDatabase();
  await RethinkDB.table(internals.kTableName).indexDrop(internals.kPrimaryName + '-' + internals.kDateName).run(connection);
};
