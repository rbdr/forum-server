'use strict';

const internals = {
  kTableName: 'forums',
  kOrderKey: 'position',
  kLimitAmount: 50
};

const RethinkDB = require('rethinkdb');
const { getConnectionWithDatabase } = require('../../../db/helpers');

module.exports = async function forums(parent, args, context, info) {

  const connection = await getConnectionWithDatabase();
  const cursor = await RethinkDB
    .table(internals.kTableName)
    .orderBy({
      index: RethinkDB.asc(internals.kOrderKey)
    })
    .limit(internals.kLimitAmount)
    .run(connection);

  return await cursor.toArray();
};
