'use strict';

/**
 * Middleware to create responses for unhandled errors.
 */
module.exports = async function HandleErrors(next) {

  try {
    await next;
  }
  catch (err) {
    this.status = err.status || 500;

    const response = {
      error: err.message,
      status: this.status
    };

    if (response.status === 401) {
      response.error === 'Protected resource, use Authorization header to get access';
    }

    this.body = response;

    this.app.emit('error', err, this);
  }
};
