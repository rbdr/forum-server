'use strict';

const RethinkDB = require('rethinkdb');
const { getConnectionWithDatabase } = require('../db/helpers');

const internals = {
  kTableName: 'forums',
  kIndexName: 'position'
};

module.exports.up = async function () {

  const connection = await getConnectionWithDatabase();
  await RethinkDB.table(internals.kTableName).indexCreate(internals.kIndexName).run(connection);
};

module.exports.down = async function () {

  const connection = await getConnectionWithDatabase();
  await RethinkDB.table(internals.kTableName).indexDrop(internals.kIndexName).run(connection);
};
