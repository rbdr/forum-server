'use strict';

module.exports = {
  forums: [
    {
      id: 'life',
      glyph: '☆',
      label: 'forum.name.life',
      position: 0
    },
    {
      id: 'the-world',
      glyph: '◯',
      label: 'forum.name.the_world',
      position: 1
    },
    {
      id: 'online',
      glyph: '⏀',
      label: 'forum.name.online',
      position: 2
    },
    {
      id: 'experience',
      glyph: '♢',
      label: 'forum.name.experience',
      position: 3
    },
    {
      id: 'belief',
      glyph: '⏃',
      label: 'forum.name.belief',
      position: 4
    },
    {
      id: 'movement',
      glyph: '▷',
      label: 'forum.name.movement',
      position: 5
    },
    {
      id: 'emotion',
      glyph: '☽',
      label: 'forum.name.emotion',
      position: 6
    },
    {
      id: 'interaction',
      glyph: '〒',
      label: 'forum.name.interaction',
      position: 7
    },
    {
      id: 'structure',
      glyph: '▢',
      label: 'forum.name.structure',
      position: 8
    },
    {
      id: 'sound',
      glyph: '〰',
      label: 'forum.name.sound',
      position: 9
    },
    {
      id: 'words',
      glyph: '╳',
      label: 'forum.name.words',
      position: 10
    },
    {
      id: 'us',
      glyph: '╱',
      label: 'forum.name.us',
      position: 11
    },
    {
      id: 'everything',
      glyph: '♡',
      label: 'forum.name.everything',
      position: 12
    }
  ],
  topics: [
    {
      id: '01a3d641-d094-4d42-a4c9-ab48d8c0725a',
      forum_id: 'everything',
      tags: {
        question: 5,
        meta: 34,
        carrots: 1,
        tpbo: 2
      },
      title: 'This is a topic that has both topics and tags',
      created_at: Date.now(),
      updated_at: Date.now(),
      ttl: 3 * 24 * 60 * 60 * 1000 // 3 days TTL
    },
    {
      id: '4d7b88cb-36af-4ea5-b61c-180c444b2836',
      forum_id: 'everything',
      title: 'This is a topic that only has a forum',
      created_at: Date.now(),
      updated_at: Date.now(),
      ttl: 60 * 60 * 1000 + 60 * 1000 // 1h1m TTL
    },
    {
      id: '0954e660-f327-4f58-aba1-8750a57cc419',
      title: 'This is a post that only has tags',
      tags: {
        meta: 20,
        question: 1
      },
      created_at: Date.now(),
      updated_at: Date.now(),
      ttl: 3 * 24 * 60 * 60 * 1000 // 3 days TTL
    },
    {
      id: '21f792db-ad6b-4d56-aed8-2fc795f77aea',
      title: 'This is an unlisted post',
      created_at: Date.now(),
      updated_at: Date.now(),
      ttl: 3 * 24 * 60 * 60 * 1000 // 3 days TTL
    }
  ],
  posts: [
    {
      id: '0e8f0d22-d240-40ee-92dd-5b18e7b41d5d',
      topic_id: '01a3d641-d094-4d42-a4c9-ab48d8c0725a',
      author_id: '2350cf30-7746-4e17-a3ca-11b86a503ac2',
      text: 'this is just a simple post',
      created_at: Date.now()
    },
    {
      id: '3f5de7d7-a68e-413e-8b3a-2fdc5682ed2f',
      topic_id: '01a3d641-d094-4d42-a4c9-ab48d8c0725a',
      author_id: '9a2a7771-2d70-421d-aa19-11c7d477e4c1',
      text: 'this is just a simple post',
      created_at: Date.now() + 15 * 60 * 1000
    },
    {
      id: '9e468d8e-1fef-43a1-8e8f-c8734aaaf4cc',
      topic_id: '4d7b88cb-36af-4ea5-b61c-180c444b2836',
      author_id: '2f0c4f0c-f42e-4fda-93e6-b6fdfb06d9b1',
      text: 'this is an even simpler post',
      created_at: Date.now()
    },
    {
      id: 'e2914839-a12f-4a30-8d4a-20dbc27bc6c3',
      topic_id: '0954e660-f327-4f58-aba1-8750a57cc419',
      author_id: '511586ad-5213-468d-8b24-862aeac23326',
      text: 'This is an OK post.\n\nWith some newlines in there.\ncool.',
      created_at: Date.now()
    },
    {
      id: 'ff0fead0-1298-48ec-a320-be9332884403',
      topic_id: '21f792db-ad6b-4d56-aed8-2fc795f77aea',
      author_id: 'da556604-65ea-4e54-990a-ca478aadd3ef',
      text: 'This post is secret.',
      created_at: Date.now()
    }
  ],
  users: [
    {
      id: '2350cf30-7746-4e17-a3ca-11b86a503ac2',
      handle: 'time4carrots'
    },
    {
      id: '9a2a7771-2d70-421d-aa19-11c7d477e4c1',
      handle: 'elevatormusic666'
    },
    {
      id: '2f0c4f0c-f42e-4fda-93e6-b6fdfb06d9b1',
      handle: 'xXRIPXx182'
    },
    {
      id: '511586ad-5213-468d-8b24-862aeac23326',
      handle: 'OK'
    },
    {
      id: 'da556604-65ea-4e54-990a-ca478aadd3ef',
      handle: 'props'
    }
  ]
};
